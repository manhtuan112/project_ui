import React from 'react'
import { Row, Col, Container, Table, Button } from "react-bootstrap";

function DeviceInfo() {
  const data = {
    'device_id': 'HS_PE100A_1',
    'os_name': 'Ubuntu',
    'os_version': '22.04',
    'fw_version': '0.1.3'
  }
  return (
    <Container fluid>
      <h3 className="mb-4">
        Device Info
      </h3>
      <Row className="mb-5 ms-4">
        <Col xs={12} className='mb-3'>
          <h4>Device</h4>
          Device ID: {data.device_id}
        </Col>
        <Col xs={12} className='mb-3'>
          <h4>Operation System</h4>
          <Col>
            OS Name: {data.os_name}
          </Col>
          <Col>
            OS Version: {data.os_version}
          </Col>
        </Col>

        <Col xs={12} className='mb-3'>
          <h4>Fileware</h4>
          FW Version: {data.fw_version}
        </Col>

        <Col xs={12} className='mb-3'>
          <h4>Other device</h4>
        </Col>
      </Row>
    </Container >
  )
}

export default DeviceInfo