import React from "react";
const Containers = React.lazy(() => import('views/admin/Containers'));
const Images = React.lazy(() => import('views/admin/Images'));
const Volumes = React.lazy(() => import('views/admin/Volumes'));
const DeviceInfo = React.lazy(() => import('views/admin/DeviceInfo'));
const HardwareInfo = React.lazy(() => import('views/admin/HardwareInfo'));


const adminRoute = [
    {
        path: '/device-info',
        exact: true,
        name: "DeviceInfo Page",
        element: <DeviceInfo />
    },
    {
        path: '/hardware-info',
        exact: true,
        name: "HardwareInfo Page",
        element: <HardwareInfo />
    },
    {
        path: '/containers',
        exact: true,
        name: "Containers Page",
        element: <Containers />
    },
    {
        path: '/images',
        exact: true,
        name: "Images Page",
        element: <Images />
    },
    {
        path: '/volumes',
        exact: true,
        name: "Volumes Page",
        element: <Volumes />
    },
]

export default adminRoute